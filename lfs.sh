#!/bin/bash

# Minimum file size in bytes (100MB)
min_size=$((100 * 1024 * 1024))

# Check if a file is already tracked by Git LFS
is_tracked_by_lfs() {
    local file=$1
    grep -q "^${file} filter=lfs" .gitattributes
}

# Find files larger than or equal to the minimum size, ignoring the .git directory
find . -type f -size +${min_size}c -not -path "./.git/*" | while read -r file; do
    if is_tracked_by_lfs "$file"; then
        echo "File already tracked by LFS: $file"
    else
        git lfs track "$file"
        echo "Added $file to Git LFS"
    fi
done
